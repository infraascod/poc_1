provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "demostate-file-bucket"
    region = "us-east-1"
    key = "Global/terraform.tfstate"	
  }
}