output "instance-pub-public_ip" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = module.ec2.pub_inst_pub_ip
}

output "instance-pvt-public_ip" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = module.ec2.pub_inst_pvt_ip
}

output "instance-pub-private_ip" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = module.ec2.pvt_inst_pub_ip
}

output "instance-pvt-private_ip" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = module.ec2.pvt_inst_pvt_ip
}

output "eip" {
  value = module.vpc.eip
}