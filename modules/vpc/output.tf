output "vpc" {
  value = aws_vpc.vpc.id
}

output "sg_pub_id" {
  value = aws_security_group.sg-pub.id
}

output "sg_priv_id" {
  value = aws_security_group.sg-pvt.id
}

output "avilabilityZone" {
    value = data.aws_availability_zones.available
  
}

output "public_subnet" {
  value = aws_subnet.subnet-public.id
  
}

output "private_subnet" {
  value = aws_subnet.private_subnet.id
  
}

output "eip" {
  value = aws_eip.eip.public_ip
  
}