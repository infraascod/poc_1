variable "namespace" {
  type        = string
}
variable "vpc-cidr-block" {
  description = "The CIDR block to associate to the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public-subnet-cidr-block" {
  description = "The CIDR block to associate to the subnet"
  type        = list(string)
  default     = ["10.0.1.0/24"]
}

variable "private-subnet-cidr-block" {
  description = "The CIDR block to associate to the subnet"
  type        = list(string)
  default     = ["10.0.2.0/24"]
}

variable "ingress_rules-public" {
  description = "Port for each service"
  type        = list(list(string))
  default = [
      ["22", "22", "tcp", "0.0.0.0/0", "ssh"],
      ["80", "80", "tcp", "0.0.0.0/0", "http"],
      ["443", "443", "tcp", "0.0.0.0/0", "https"]
  ]
}

variable "ingress_rules-private" {
  description = "Port for each service"
  type        = list(list(string))
  default = [
      ["22", "22", "tcp", "0.0.0.0/0", "ssh"],
  ]
}