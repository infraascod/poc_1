data "aws_availability_zones" "available" {}

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc-cidr-block
  enable_dns_hostnames = true

  tags = {
    Name = "${var.namespace}-VPC"
  }
}

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.namespace}-IGW"
  }
}

resource "aws_subnet" "subnet-public" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.public-subnet-cidr-block[0]
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.namespace}-SUBNET-PUBLIC"
  }
}

resource "aws_route_table" "rt-pub" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig.id
  }

  tags = {
    Name = "${var.namespace}-RT-PUB"
  }
}

resource "aws_route_table_association" "rta-pub" {
  subnet_id      = aws_subnet.subnet-public.id
  route_table_id = aws_route_table.rt-pub.id
}

resource "aws_security_group" "sg-pub" {
  name   = "${var.namespace}-VPC-SG-PUB"
  vpc_id = aws_vpc.vpc.id

  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    Name = "${var.namespace}-SG-PUB"
  }
}

resource "aws_security_group_rule" "ingress_rules-public" {
  count             = length(var.ingress_rules-public)
  type              = "ingress"
  from_port         = var.ingress_rules-public[count.index][0]
  to_port           = var.ingress_rules-public[count.index][1]
  protocol          = var.ingress_rules-public[count.index][2]
  cidr_blocks       = [var.ingress_rules-public[count.index][3]]
  description       = var.ingress_rules-public[count.index][4]
  security_group_id = aws_security_group.sg-pub.id

}

//Private subnet 
resource "aws_subnet" "private_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private-subnet-cidr-block[0]
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "${var.namespace}-SUBNET-PRIVATE"
  }  
}

resource "aws_eip" "eip" {
  vpc      = true

  tags = {
    Name = "${var.namespace}-EIP"
  }

}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.eip.id
  subnet_id = aws_subnet.subnet-public.id
  tags = {
    name = "${var.namespace}-NAT-GW"
  }
  
}

resource "aws_route_table" "rt-pvt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "${var.namespace}-RT-PVT"
  }
}

resource "aws_route_table_association" "rta-pvt" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.rt-pvt.id

}

resource "aws_security_group" "sg-pvt" {
  name   = "${var.namespace}-VPC-SG-PVT"
  vpc_id = "${aws_vpc.vpc.id}"
  
  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    Name = "${var.namespace}-SG-PVT"
  }
}

resource "aws_security_group_rule" "ingress_rules-Private" {
  count             = length(var.ingress_rules-private)
  type              = "ingress"
  from_port         = var.ingress_rules-private[count.index][0]
  to_port           = var.ingress_rules-private[count.index][1]
  protocol          = var.ingress_rules-private[count.index][2]
  cidr_blocks       = [var.ingress_rules-private[count.index][3]]
  description       = var.ingress_rules-private[count.index][4]
  security_group_id = aws_security_group.sg-pvt.id

}