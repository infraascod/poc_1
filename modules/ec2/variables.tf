variable "vpc" {
  type = string
}

variable key_name {
  type = string
}

variable "sg_pub_id" {
  type = string
}

variable "sg_priv_id" {
  type = string
}

variable "public_subnet" {
  type = string
  
}

variable "private_subnet" {
  type = string
  
}

variable "instance-type-pub" {
  description = "The instance type to be used"
  type        = string
  default     = "t2.micro"
}

variable "instance-type-pvt" {
  description = "The instance type to be used"
  type        = string
  default     = "t2.micro"
}

variable "instance-associate-public-ip" {
  description = "Defines if the EC2 instance has a public IP address."
  type        = string
  default     = "true"
}

variable "namespace" {
  description = "The project namespace to use for unique resource naming"
  type        = string
}

variable "inst_name" {
  description = "The name of the instance or specfic purpose of instance"
  type        = list(string)
  default = ["testing-pub", "testing-pvt"]
}

variable "key_pair" {
  description = "AWS key pair"
  type        = string
}