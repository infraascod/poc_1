output "pub_inst_pub_ip" {
  value = aws_instance.instance-pub.public_ip
}

output "pub_inst_pvt_ip" {
  value = aws_instance.instance-pub.private_ip
}

output "pvt_inst_pub_ip" {
  value = aws_instance.instance-pvt.public_ip
}

output "pvt_inst_pvt_ip" {
  value = aws_instance.instance-pvt.private_ip
}