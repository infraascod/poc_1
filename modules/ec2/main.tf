
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "instance-pub" {
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = var.instance-type-pub
  key_name                    = var.key_name
  associate_public_ip_address = var.instance-associate-public-ip
  vpc_security_group_ids      = [var.sg_pub_id]
  subnet_id                   = var.public_subnet

  tags = {
    Name = "${var.namespace}-${var.inst_name[0]}-PUBLIC"
  }
}

resource "aws_instance" "instance-pvt" {
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = var.instance-type-pvt
  key_name                    = var.key_name
  associate_public_ip_address = "false"
  vpc_security_group_ids      = [var.sg_priv_id]
  subnet_id                   = var.private_subnet
  
  tags = {
    Name = "${var.namespace}-${var.inst_name[1]}-PRIVATE"
  }
}