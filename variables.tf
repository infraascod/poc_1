variable "namespace" {
  description = "The project namespace to use for unique resource naming"
  default     = "EC2_VPC"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}

variable "key_name" {
  description = "AWS key pair"
  default     = "us-east-1"
  type        = string
}