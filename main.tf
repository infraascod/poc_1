
module "vpc" {
  source    = "./modules/vpc"
  namespace = var.namespace
}

module "ec2" {
  source     = "./modules/ec2"
  namespace  = var.namespace
  key_name = var.key_name
  key_pair = var.key_name
  vpc        = module.vpc.vpc
  sg_pub_id  = module.vpc.sg_pub_id
  sg_priv_id = module.vpc.sg_priv_id
  public_subnet = module.vpc.public_subnet
  private_subnet = module.vpc.private_subnet

}
